@extends('layouts.layout')

@section('title')
  Create New Product
@stop

@section('body')
  {!!Form::open(['route' => 'product.store']) !!}

  {!!Form::label('name', 'Name')!!}
  {!!Form::text('name', null, ['placeholder' => "Give a name"])!!}

  {!!Form::label('price', 'Price')!!}
  {!!Form::text('price', null, ['placeholder' => "Give a price"])!!}

  {!!Form::submit('Create')!!}

  {!! Form::close() !!}
@stop
