@extends('layouts.layout')

@section('title')
  {$product->name}
@stop

@section('body')
  {!! Form::open([
    'method' => 'delete',
    'route' => ['product.destroy', $product->id]
    ])!!}
  <h1>{{$product->name}}</h1>
  <h3>{{$product->price}}</h3>

  <pre>
    <a href="{{route('product.edit', $product->id)}}">Edit</a>  {!!Form::submit('Delete')!!}
    <!-- <a href="{{route('product.destroy', $product->id)}}">Delete</a> -->
  </pre>
  {!!Form::close()!!}
@stop
