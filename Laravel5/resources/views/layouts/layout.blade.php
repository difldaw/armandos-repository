<!DOCTYPE html>
<html>
  <head>
    <title>@yield('title')</title>
  </head>

  <body>
    @include('layouts.menu')
    <!--@include('layouts.sidebarleft')-->
    @yield('body')
  </body>
</html>
